@extends('layouts.app')

@section('content')

<div class="main-grids">
    <div class="top-grids">
        <div class="recommended-info">
            <h3>Videos Recientes</h3>
        </div>

    @if(count($videos) > 0)

        <div class="recommended-grids english-grid">
		
            @foreach($videos as $video)
                <div class="col-md-4 resent-grid recommended-grid slider-top-grids" style="margin-bottom:20px">
                    <div class="resent-grid-img recommended-grid-img">
                        <a href="{{ url('/verVideo/'.$video->id) }}"><img src="{{ asset($video->ruta.$video->nombreArchivo) }}" height="237" width="356"/></a>
                        <div class="time">
                            <p>{{$video->id}}</p>
                        </div>
                        <div class="clck">
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="resent-grid-info recommended-grid-info">
                        <h3><a href="{{ url('/verVideo/'.$video->id) }}" class="title title-info">{{ $video->titulo }}</a></h3>
                        <ul>
                            <li><p class="author author-info"><a href="#" class="author">{{$video->name}}</a></p></li>
                            <li class="right-list"><p class="views views-info">0 vistas</p></li>
                        </ul>
                    </div>
                </div>
            @endforeach
            <div class="clearfix"> </div>
        </div>
    @else

            <p>No se encontraron videos</p>

    @endif

    </div>
</div>

@endsection