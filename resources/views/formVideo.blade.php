@extends('layouts.app')

@section('content')

<div class="container">

<h2>Subir video</h2>
<br/>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<br/>
<form action="{{ url('/createVideo') }}" enctype="multipart/form-data" role="form" method="POST">
  @csrf
  <div class="form-group">
    <label for="titulo">Título</label>
    <input type="text" class="form-control" name="titulo" id="titulo" aria-describedby="titulo" placeholder="Ingrese el título del video" autofocus>
  </div>
  <div class="form-group">
    <label for="titulo">Descripción</label>
    <input type="text" class="form-control" name="descripcion" id="descripcion" aria-describedby="decripcion" placeholder="Ingrese el descripción del video">
  </div>
  <div class="form-group">
    <label for="miniatura">Miniatura</label>
    <input type="file" class="form-control-file" id="miniatura" name="miniatura">
  </div>
  <div class="form-group">
    <label for="video">Video</label>
    <input type="file" class="form-control-file" id="video" name="video">
  </div>
  <button type="submit" class="btn btn-primary">Guardar</button>
</form>

</div>

@endsection