@extends('layouts.app')

@section('content')

<div class="container">

<h1>Editar video</h1>

<form action="{{ url('/editVideo/') }}" enctype="multipart/form-data" role="form" method="POST">
  @csrf
  <input id="id" name="id" id="id" type="hidden" value="{{ $video->id }}">
  <div class="form-group">
    <label for="titulo">Título</label>
    <input type="text" class="form-control" name="titulo" id="titulo" aria-describedby="titulo" value="{{$video->titulo}}">
  </div>
  <div class="form-group">
    <label for="titulo">Descripción</label>
    <input type="text" class="form-control" name="descripcion" id="descripcion" aria-describedby="decripcion" value="{{$video->titulo}}">
  </div>
  <button type="submit" class="btn btn-primary">Modificar</button>
</form>

</div>

@endsection