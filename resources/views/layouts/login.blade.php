<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel='stylesheet' type='text/css' media="all" />
    <!-- //bootstrap -->
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
    <!-- Custom Theme files -->
    <link href="{{ asset('css/style.css?v=2') }}" rel='stylesheet' type='text/css' media="all" />
    <script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <!--start-smoth-scrolling-->
    <!-- fonts -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
    <!-- //fonts -->

</head>
<body>
    <div id="app">

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">
                    <h1><img src="{{ asset('images/logo.png') }}" alt="" /></h1>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <div class="header-top-right">
                    @guest
                        <div class="signin">
                            <a href="{{ route('login') }}" class="play-icon popup-with-zoom-anim">Iniciar Sesión</a>
                        </div>

                        @if (Route::has('register'))
                            <div class="signin">
                                <a href="{{ route('register') }}" class="play-icon popup-with-zoom-anim">Registrarse </a>
                                <!-- pop-up-box -->
                            </div>
                        @endif
                    @else
                        <div class="signin">
                            <a href="{{ url('/nuevoVideo/') }}"> <span class="glyphicon glyphicon-facetime-video"></span> Subir Video</a>
                        </div>	
                    @endguest
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </nav>

    <div class="col-sm-12 col-md-12 main">
        <!-- content -->
        @yield('content')
    </div>
	<div class="clearfix"> </div>
	<div class="drop-menu">
		<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu4">
		  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Regular link</a></li>
		  <li role="presentation" class="disabled"><a role="menuitem" tabindex="-1" href="#">Disabled link</a></li>
		  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another link</a></li>
		</ul>
	</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    </div>
</body>
</html>
