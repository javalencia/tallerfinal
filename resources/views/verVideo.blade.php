@extends('layouts.app')

@section('content')
     @if($video)
          <div class="col-sm-8 single-left">
               <div class="song">
                    <div class="song-info">
                         <h3>{{ $video->titulo }}</h3>	
                    </div>
                    <div class="video-grid">
                         <video src="{{ asset($video->ruta.$video->nombreArchivo) }}" controls style="height:376px;width:100%"></video>
                    </div>
               </div>
               <div class="song-grid-right">
                    <div class="share">
                         <h5>Compartir</h5>
                         <ul>
                              <li><a href="#" class="icon fb-icon">Facebook</a></li>
                              <li><a href="#" class="icon dribbble-icon">Dribbble</a></li>
                              <li><a href="#" class="icon twitter-icon">Twitter</a></li>
                              <li><a href="#" class="icon pinterest-icon">Pinterest</a></li>
                              <li><a href="#" class="icon whatsapp-icon">Whatsapp</a></li>
                              <li><a href="#" class="icon like">Like</a></li>
                              <li><a href="#" class="icon comment-icon">Comments</a></li>
                              <li class="view">200 Views</li>
                         </ul>
                    </div>
               </div>
               <div class="clearfix"> </div>
               <div class="published">
                    <div class="load_more">	
                         <ul id="myList">
                              <li style="display: list-item;">
                                   <h4>Publicado: {{$video->created_at}}</h4>
                                   <p>{{$video->descripcion}}</p>
                              </li>
                         </ul>
                    </div>
               </div>
               <div class="all-comments">
                    <div class="all-comments-info">
                         @php
                              $posts = DB::table('posts')
                                      ->join('users', 'posts.users_id', '=', 'users.id')
                                     ->where('videos_id',$video->id)
                                       ->get()
                         @endphp
                         <a href="#">Todos los comentarios ({{count($posts)}})</a>
                         <div class="box">
                              <form action="{{ url('/nuevoPost') }}"  method="POST">
                                   <input id="prodId" name="videos_id" id="videos_id" type="hidden" value="{{ $video->id }}">
                                   <textarea required=" " name="post" id="post" aria-describedby="post" placeholder="Ingrese comentario del video" rows="4"></textarea>
                                   <button type="submit" class="btn btn-primary">Enviar</button>
                                   <div class="clearfix"> </div>
                              </form>
                         </div>
                    </div>

                    @if(Auth::user()->admin == 1)
                         <div class="all-comments-buttons">
                              <ul>
                                   <li><a href="{{ url('/editarVideo/'.$video->id) }}" class="top">Editar Video</a></li>
                                   <li><a href="{{ url('/eliminarVideo/'.$video->id) }}" class="top newest">Borrar Video</a></li>
                              </ul>
                         </div>
                    @endif

                    <div class="media-grids">
                         @if(count($posts)> 0)
                              @foreach($posts as $post)
                                   <div class="media">
                                        <h5>{{$post->name}}</h5>
                                        <div class="media-left">
                                             <a href="#">
                                                  
                                             </a>
                                        </div>
                                        <div class="media-body">
                                             <p>{{ $post->comentario }}</p>
                                             <span>Creación:<a href="#"> {{$post->created_at}} </a></span>
                                        </div>
                                   </div>

                              @endforeach
                         @endif
                         
                    </div>
               </div>
          </div>  
     @else
          <p>No se encontró el video</p>
     @endif
@endsection