<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'VideoController@verVideos');
    Route::get('/home', 'VideoController@verVideos')->name('home');
    Route::get('nuevoVideo','VideoController@formVideo');
    Route::post('createVideo','VideoController@createVideo');
    Route::get('verVideos','VideoController@verVideos')->name('verVideos');
    Route::post('nuevoPost','PostController@nuevoPost');
    Route::get('eliminarVideo/{id}','VideoController@eliminarVideo');
    Route::get('editarVideo/{id}','VideoController@editarVideo');
    Route::post('editVideo','VideoController@editVideo');
    Route::get('verVideo/{id}','VideoController@verVideo')->name('verVideo');
});


