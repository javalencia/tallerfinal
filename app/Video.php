<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
     * The table associated with the model.
     */
    protected $table = 'videos';

    /**
     * The primary key associated with the table. 
     */
    protected $primaryKey = 'id';

     /**
     * Indicates if the model should be timestamped.
     */
    public $timestamps = false;
}
