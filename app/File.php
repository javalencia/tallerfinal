<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * The table associated with the model.
     */
    protected $table = 'files';

    /**
     * The primary key associated with the table. 
     */
    protected $primaryKey = 'id';

     /**
     * Indicates if the model should be timestamped.
     */
    public $timestamps = false;
}
