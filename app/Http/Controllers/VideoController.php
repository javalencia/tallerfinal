<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Video;
use App\File;
use DB;


class VideoController extends Controller
{
    /**
     * Show the form to create video.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function formVideo()
    {
        return view('formVideo');
    }
    
    /** 
     * Save the video 
    */
    public function createVideo(Request $request)
    {

        // Get the currently authenticated user's ID...
        $id = Auth::id();

       request()->validate([
            'titulo' => 'required|min:5|max:50',
            'descripcion' => 'required|min:3|max:250',
            'miniatura' => 'image|mimes:jpg|max:2048',
            'video' => 'mimes:mp4'
       ]);

       $video = new Video();
       $video->titulo = $request->input('titulo');
       $video->descripcion = $request->input('descripcion');
       $video->save();


       if ($files = $request->file('miniatura')) {
           $destinationPath = 'public/miniatura/'; // upload path
           $profileImage =  $video->id. "-" .$video->titulo. "." . $files->getClientOriginalExtension();
           
           $archivo = new File();
           $archivo->nombreArchivo	= $profileImage;
           $archivo->ruta	= $destinationPath;
           $archivo->miniatura = 1;
           $archivo->videos_id = $video->id;
           $archivo->users_id = $id;
           $archivo->save();

           $files->move($destinationPath, $profileImage);
        }

        if ($files = $request->file('video')) {
            $destinationPath = 'public/video/'; // upload path
            $profileImage = $video->id. "-" .$video->titulo. "." . $files->getClientOriginalExtension();

            $archivo = new File();
            $archivo->nombreArchivo	= $profileImage;
            $archivo->ruta	= $destinationPath;
            $archivo->video = 1;
            $archivo->videos_id = $video->id;
            $archivo->users_id = $id;
            $archivo->save();

            $files->move($destinationPath, $profileImage);
         }

        return redirect()->route('verVideos');
    }

    public function verVideos()
    {
        $videos = DB::table('videos')
                   ->select('videos.id','titulo','descripcion','nombreArchivo','ruta', 'name')
                     ->leftJoin('files', 'videos.id', '=', 'files.videos_id')
                     ->leftJoin('users', 'files.users_id', '=', 'users.id')
                    ->where('files.miniatura',1)
                      ->get();

        return view('videos')
                ->with('videos', $videos); 
    }

    public function eliminarVideo($id)
    {
        Video::where('id',$id)->delete();

        return redirect()->route('verVideos');
    }

    public function editarVideo($id)
    {
        $video = Video::select('id','titulo','descripcion')
                    ->where('id', $id)
                    ->first();

        return view('editarVideo')
                ->with('video', $video);

    }

    public function editVideo(Request $request)
    {
        Video::where('id',$request->input('id'))
        ->update([ 'titulo' =>  $request->input('titulo'),
        'descripcion' =>  $request->input('descripcion')
        ]);  
        
        return redirect()->route('verVideo', $request->input('id'));
    }

    public function verVideo($id)
    {
        $video = DB::table('files')
                    ->join('videos', 'files.videos_id', '=', 'videos.id')
                   ->where('video', 1)
                   ->where('videos_id',$id)
                   ->first();

        return view('verVideo')
             ->with('video', $video);
    }
     


}
