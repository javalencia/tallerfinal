<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use DB;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{

    public function nuevoPost(Request $request)
    {
        // Get the currently authenticated user's ID...
        $id = Auth::id();

        $post = new Post();
        $post->comentario = $request->input('post');
        $post->videos_id = $request->input('videos_id');
        $post->users_id = $id;
        $post->save();

        $video = DB::table('files')
                    ->join('videos', 'files.videos_id', '=', 'videos.id')
                   ->where('video', 1)
                   ->where('videos_id', $request->input('videos_id'))
                   ->first();

        return view('verVideo')
            ->with('video', $video);
    }
}
